<?php
/**
 * @package WiseCore
 */

namespace WiseCore\Inc\Base;

use \WiseCore\Inc\Base\BaseController;

class Enqueue extends BaseController{

    function register() {
        add_action ( 'admin_enqueue_scripts'  , array ( $this , 'admin_enqueue' ) );
        add_action ( 'wp_enqueue_scripts'  , array ( $this , 'user_enqueue' ) );
    }


    function admin_enqueue(){
        // Backend Style and Scritps
        wp_enqueue_style ( 'mystyleplugin'  , $this->plugin_url .  'assets/css/mystyle.css '  );
        wp_enqueue_script ( 'myscriptplugin'  , $this->plugin_url . 'assets/js/myscript.js '  );


    }


    function user_enqueue(){
        // Front End Styles and Scripts
        wp_enqueue_style( 'popup-style' , $this->plugin_url . 'assets/css/popup-style.css' , '' ,'1.0' ,true );
        wp_enqueue_script( 'popup-script' , $this->plugin_url . 'assets/js/popup-script.js' , array('jquery') , '1.0' , true);
    }
}
