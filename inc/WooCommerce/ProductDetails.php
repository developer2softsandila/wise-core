<?php
/**
 * @package WiseCore
 */

namespace WiseCore\Inc\WooCommerce;

use WiseCore\Inc\Base\BaseController;

class ProductDetails extends BaseController
{

    public function register()
    {

        add_filter('woocommerce_product_single_add_to_cart_text', array($this, 'wise_core_woocommerce_custom_single_add_to_cart_text'));
        add_filter('woocommerce_add_to_cart_redirect', array($this, 'wise_core_skip_cart_redirect_checkout'));
        add_filter('wc_add_to_cart_message_html', array($this, 'wise_core_remove_add_to_cart_message'));
        add_action('init',  array ( $this,  'wise_core_remove_loop_button' ) );
        add_action('woocommerce_after_add_to_cart_button', array($this, 'wise_core_after_add_to_cart_btn'));
        add_action('woocommerce_after_shop_loop_item', array($this, 'wise_core_replace_add_to_cart'));
        add_filter('woocommerce_is_sold_individually', array($this, 'wise_core_custom_remove_all_quantity_fields'), 10, 2);
        add_filter('woocommerce_add_to_cart_validation', array($this, 'wise_core_only_one_in_cart'), 99, 2);
        add_filter('gettext', array($this, 'wise_core_related_products_strings'), 20, 3);
        add_action( 'init', array ( $this , 'cptui_register_my_taxes' ) );
        add_action( 'init', array (  $this , 'cptui_register_my_taxes_gender' ) );

    }

    // To change add to cart text on single product page
    function wise_core_woocommerce_custom_single_add_to_cart_text()
    {
        return 'RESERVE NOW';
    }

    function wise_core_skip_cart_redirect_checkout($url)
    {
        return wc_get_checkout_url();
    }

    function wise_core_remove_add_to_cart_message($message)
    {
        return '';
    }


    /*STEP 1 - REMOVE ADD TO CART BUTTON ON PRODUCT ARCHIVE (SHOP) */

    function wise_core_remove_loop_button()
    {
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price' );
    }


    function wise_core_after_add_to_cart_btn()
    {
        global $product;
        ?>
        <div class="container">
            <?php
            global $product;
            $_date_of_birth = get_field("_date_of_birth", $product->get_id());
            $start = strtotime($_date_of_birth);
            $end = strtotime(date("Y/m/d"));
            $days_between = ceil(abs($end - $start) / 86400);
            $number_of_Weeks = round($days_between / 7);
        ?>
        <div class="puppy-primary-info clearfix">

            <ul class="puppy-detail-chart">
                <li><span>
                        <label>Age</label>
                        <p class="nobr">
                            <?php echo ($number_of_Weeks) ? $number_of_Weeks .' Weeks' : 'N/A'; ?>
                        </p>
                    </span>
                </li>
                <li>
                    <span>
                        <?php
                        $gender = wp_get_post_terms($product->get_id() , 'gender', true );


                        ?>
                        <label>Gender</label>
                        <p><?php echo ( $gender[0]->name ) ? $gender[0]->name : 'N/A' ; ?></p>
                    </span>
                </li>
                <li>
                    <span>
                        <label>Price</label>
                        <p>
                            <?php echo $product->get_price_html();?>
                        </p>
                    </span>
                </li>
            </ul>
        </div>
            <div class="wrapper center-block">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading active" role="tab" id="headingOne">
                            <h3 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Puppy Facts
                                </a>
                            </h3>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="row">
                                    <div class="col-md-12">

                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <?php
                                            $puppy_id = get_field("puppy_id", $product->get_id());
                                            ?>
                                            <th scope="row">Puppy ID:</th>
                                            <td><?php echo ($puppy_id) ? $puppy_id : 'N/A'; ?></td>
                                            <?php
                                            $_date_available = get_field("_date_available", $product->get_id());
                                            ?>
                                            <th scope="row">Date Available:</th>
                                            <td><?php echo ($_date_available) ? $_date_available : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $variety = get_field("variety", $product->get_id());
                                            ?>
                                            <th scope="row">Variety:</th>
                                            <td><?php echo ($variety) ? $variety : 'N/A'; ?></td>
                                            <?php
                                            $color = get_field("color", $product->get_id());
                                            ?>
                                            <th scope="row">Color:</th>
                                            <td><?php echo ($color) ? $color : 'N/A'; ?></td>

                                        </tr>
                                        <tr>
                                            <?php
                                            $dads_weight = get_field("dads_weight", $product->get_id());
                                            ?>
                                            <th scope="row">Dad's Weight:</th>
                                            <td><?php echo ($dads_weight) ? $dads_weight : 'N/A'; ?></td>
                                            <?php
                                            $moms_weight = get_field("moms_weight", $product->get_id());
                                            ?>
                                            <th scope="row">Mom's Weight:</th>
                                            <td><?php echo ($moms_weight) ? $moms_weight : 'N/A'; ?></td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $registry = get_field("registry", $product->get_id());
                                            ?>
                                            <th scope="row">Registry:</th>
                                            <td><?php echo ($registry) ? $registry : 'N/A'; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php
    }

    /*STEP 2 -ADD NEW BUTTON THAT LINKS TO PRODUCT PAGE FOR EACH PRODUCT */

    function wise_core_replace_add_to_cart()
    {
        global $product;
        $link = $product->get_permalink(); ?>
        <a href="<?php echo esc_attr($link) ?>" class="btn btn-primary pull-center proceed-to-checkout">Learn more about
            me</a>
        <?php
    }


    function wise_core_custom_remove_all_quantity_fields($return, $product)
    {
        return TRUE;
    }


    function wise_core_only_one_in_cart($passed, $added_product_id)
    {
        wc_empty_cart();
        return $passed;
    }


    // Change WooCommerce "Related products" text

    function wise_core_related_products_strings( $translated_text, $text, $domain )
    {
        switch ($translated_text) {

            case 'Related Products' :
                $translated_text = __('Available Puppies', 'woocommerce');
                break;
        }
        return $translated_text;
    }



    function wise_core_woocommerce_hide_product_price( $price ) {
            return '';
    }


    function cptui_register_my_taxes() {

        /**
         * Taxonomy: Gender.
         */

        $labels = [
            "name" => __( "Gender", "Porto-child" ),
            "singular_name" => __( "Gender", "Porto-child" ),
        ];

        $args = [
            "label" => __( "Gender", "Porto-child" ),
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => true,
            "hierarchical" => false,
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => [ 'slug' => 'gender', 'with_front' => true, ],
            "show_admin_column" => false,
            "show_in_rest" => true,
            "rest_base" => "gender",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit" => false,
        ];
        register_taxonomy( "gender", [ "product" ], $args );
    }

    function cptui_register_my_taxes_gender() {

        /**
         * Taxonomy: Gender.
         */

        $labels = [
            "name" => __( "Gender", "Porto-child" ),
            "singular_name" => __( "Gender", "Porto-child" ),
        ];

        $args = [
            "label" => __( "Gender", "Porto-child" ),
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => true,
            "hierarchical" => false,
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => [ 'slug' => 'gender', 'with_front' => true, ],
            "show_admin_column" => false,
            "show_in_rest" => true,
            "rest_base" => "gender",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit" => false,
        ];
        register_taxonomy( "gender", [ "product" ], $args );
    }

}
