<?php


/**
 * @package WiseCore
 */

namespace WiseCore\Inc\WooCommerce;

use WiseCore\Inc\Base\BaseController;

class ShortCodeFilter extends BaseController
{

    public function register()
    {
        // Porto Widget Woo Products
        add_shortcode( 'wise_products_gender_filter', array ( $this , 'wise_shortcode_products_filter' ) );
        add_shortcode( 'init', array ( $this , 'filter_query' ) );
    }

    public function wise_shortcode_products_filter() {
        ob_start();
        ?>
            <div>
                <fieldset>
                    <input type="checkbox" name="gender" class="gender" value="female">Female<br>
                    <input type="checkbox" name="gender" class="gender" value="male">Male<br>
                    <br>
                </fieldset>
            </div>
            <script>

            </script>
        <?php
        return ob_get_clean();
    }

    public function filter_query(){
        echo '<pre>';
        print_r ( $_GET['gender'] );
        die;
    }

}


?>
<section data-particle_enable="false" class="elementor-element elementor-element-7f58355 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7f58355" data-element_type="section">
    <div class="elementor-container elementor-column-gap-extended">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-f704d9e elementor-column elementor-col-33 elementor-top-column" data-id="f704d9e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-8f4ef40 elementor-widget elementor-widget-heading" data-id="8f4ef40" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">all blacks</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-d7f2755 elementor-widget elementor-widget-heading" data-id="d7f2755" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">Parley Range</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-c7d1d53 elementor-widget elementor-widget-eael-creative-button" data-id="c7d1d53" data-element_type="widget" data-widget_type="eael-creative-button.default">
                            <div class="elementor-widget-container">
                                <div class="eael-creative-button-wrapper">
                                    <a class="eael-creative-button eael-creative-button--antiman" href="https://allblackshop.com/product-category/all-blacks/parley-range/" data-text="Go!">
                                        <div class="creative-button-inner">
                                            <span class="cretive-button-text">Shop Now</span>
                                            <i class="fa fa-shopping-basket eael-creative-button-icon-right" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-08f15e3 elementor-column elementor-col-33 elementor-top-column" data-id="08f15e3" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-fe052e6 elementor-widget elementor-widget-heading" data-id="fe052e6" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">all blacks</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-11185fc elementor-widget elementor-widget-heading" data-id="11185fc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">RWC Range</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-f9e8d22 elementor-widget elementor-widget-eael-creative-button" data-id="f9e8d22" data-element_type="widget" data-widget_type="eael-creative-button.default">
                            <div class="elementor-widget-container">
                                <div class="eael-creative-button-wrapper">
                                    <a class="eael-creative-button eael-creative-button--antiman" href="https://allblackshop.com/rwc-y3-story/" data-text="Go!">
                                        <div class="creative-button-inner">
                                            <span class="cretive-button-text">Shop Now</span>
                                            <i class="fa fa-shopping-basket eael-creative-button-icon-right" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-f70f8a3 elementor-column elementor-col-33 elementor-top-column" data-id="f70f8a3" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-44bf1e9 elementor-widget elementor-widget-heading" data-id="44bf1e9" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">all blacks</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-829b446 elementor-widget elementor-widget-heading" data-id="829b446" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">2019 RANGE​</h2> </div>
                        </div>
                        <div class="elementor-element elementor-element-8dd7289 elementor-widget elementor-widget-eael-creative-button" data-id="8dd7289" data-element_type="widget" data-widget_type="eael-creative-button.default">
                            <div class="elementor-widget-container">
                                <div class="eael-creative-button-wrapper">
                                    <a class="eael-creative-button eael-creative-button--antiman" href="https://allblackshop.com/product-category/all-blacks/2019-range/" data-text="Go!">
                                        <div class="creative-button-inner">
                                            <span class="cretive-button-text">Shop Now</span>
                                            <i class="fa fa-shopping-basket eael-creative-button-icon-right" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






