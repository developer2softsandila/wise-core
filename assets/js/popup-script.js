(function ($) {
    $(function () {

        // Get the modal
        var modal = document.getElementById("myModal");

        var body = document.body;

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        $(window).load(function() {
            // executes when complete page is fully loaded, including all frames, objects and images
            modal.style.display = "block";
            modal.style.marginTop = "200px";
            modal.style.marginLeft = "400px";
            modal.style.width = "500px";
            modal.style.height = "300px";
            modal.style.background = "yellow";
        });


        // When the user clicks on <span> (x), close the modal
        body.onclick = function () {
            modal.style.display = "block";
            modal.style.marginTop = "100px";
            modal.style.height = "150px";
            modal.style.width = "250px";
            modal.style.marginLeft = "1000px";
            modal.style.background = "orange";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";

            }
        }

    });
})(jQuery);